package de.ur.iw.optapeb.speech_recognition_postprocessing;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sccn.LSL;
import sccn.LSL.LostException;
import sccn.LSL.StreamInfo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class SpeechTurnsReader implements Runnable {
    private final static Logger LOG = LogManager.getLogger(SpeechTurnsReader.class);

    public SpeechTurnsReader(String predicate) {
        Preconditions.checkNotNull(predicate);
        this.predicate = predicate;
    }

    /**
     * Buffer a few turns before spewing a warning message.
     */
    private static final int MIN_QUEUE_SIZE = 5;

    /**
     * We assume that the input channel has only 1 channel.
     */
    private static final int CHANNEL_INDEX = 0;

    private final String predicate;
    private final BlockingQueue<String> dataQueue = new ArrayBlockingQueue<>(MIN_QUEUE_SIZE);
    private volatile StreamInfo streamInfo = null;

    @Override
    public void run() {
        Thread.currentThread().setName("LSL stream reader");
        do {
            LSL.StreamInlet inlet = null;
            try {
                LOG.info("LSL: Calling resolve_stream(" + predicate + ")...");
                LSL.StreamInfo[] results = LSL.resolve_stream(predicate, 1, LSL.FOREVER);
                Preconditions.checkArgument(results.length > 0, "No LSL stream found matching '" + predicate + "' ...");
                LOG.info("LSL: " + results.length + " stream(s) found.");
                inlet = new LSL.StreamInlet(results[0]);
                streamInfo = inlet.info();
                LOG.info("LSL: Will use stream " + streamInfo.name() + ".");
                Thread.currentThread().setName("LSL stream reader - " + streamInfo.name());
                // System.out.println(info.as_xml());
                try {
                    /*
                    // thread-safe way of resizing the queue without data loss
                    BlockingQueue<String> newQueue = new ArrayBlockingQueue<>(MIN_QUEUE_SIZE);
                    dataQueue.drainTo(newQueue);
                    dataQueue = newQueue;
                    */

                    String[] sampleBuffer = new String[streamInfo.channel_count()];

                    do {
                        double remote_timestamp_or_zero = inlet.pull_sample(sampleBuffer, 0.5);
                        if (remote_timestamp_or_zero > 0.0) {
                            String turn = sampleBuffer[CHANNEL_INDEX];
                            LOG.info("New LSL sample: " + turn);
                            if (!dataQueue.offer(turn, 1, TimeUnit.SECONDS)) {
                                LOG.warn("Congestion: Approximately " + dataQueue.size() + " inputs are still unprocessed, will now block until they are processed.");
                                dataQueue.put(turn);
                            }
                        } else {
                            // no sample (we use a timeout)
                        }
                    } while (true);
                } finally {
                    LOG.info("Closing inlet " + streamInfo.name());
                    streamInfo = null;
                    inlet.close_stream();
                    inlet.close();

                    Thread.currentThread().setName("LSL stream reader");
                }

            } catch (LostException e) { // stream source was irrecoverably lost
                LOG.error("LSL stream source was irrecoverably lost: ", e);
            } catch (Exception e) { // unexpected, "shouldn't happen"
                LOG.error("unexpected error: ", e);
            }

            LOG.error("Stream reading has ended. Trying to re-discover a suitable stream...");

        } while (true);
    }

    @Nonnull
    public BlockingQueue<String> getDataQueue() {
        return dataQueue;
    }

    @Nonnull
    public String getPredicate() {
        return predicate;
    }

    @Nullable
    public StreamInfo getStreamInfo() {
        return streamInfo;
    }
}
