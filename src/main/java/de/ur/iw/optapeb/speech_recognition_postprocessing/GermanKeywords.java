package de.ur.iw.optapeb.speech_recognition_postprocessing;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class GermanKeywords {
    public static final Set<String> yes =
            ImmutableSet.of("ja", "ok", "positiv", "bereit");

    public static final Set<String> no =
            ImmutableSet.of("nein", "nö", "nee", "negativ");

    public static final Set<String> noChange =
            ImmutableSet.of("gleich");
}
