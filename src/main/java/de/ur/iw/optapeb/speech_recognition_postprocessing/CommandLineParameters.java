package de.ur.iw.optapeb.speech_recognition_postprocessing;

import picocli.CommandLine;

@CommandLine.Command(
        mixinStandardHelpOptions = true,
        description = "This program processes the output of the Zamia/Kaldi speech recognition module. It parses numbers and certain keywords and emits them via LSL and WAMP.",
        versionProvider = Main.class)
public class CommandLineParameters {
    @CommandLine.Option(names = "--predicate", defaultValue = "contains(name,'ZamiaSpecial') and channel_format='string'", description = "XPath predicate for selecting the LSL stream that contains the turns from the speech recognition module, defaults to ${DEFAULT-VALUE}.")
    public String lslPredicateTurns;

    @CommandLine.Option(names = "--host", defaultValue = "localhost", description = "Hostname (or IP) of the WAMP router to be used, defaults to ${DEFAULT-VALUE}.")
    public String wampRouterHostname;

    @CommandLine.Option(names = "--port", defaultValue = "8080", description = "Port of the WAMP router to be used, defaults to ${DEFAULT-VALUE}.")
    public int wampRouterPort;
}
