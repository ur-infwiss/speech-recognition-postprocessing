package de.ur.iw.optapeb.speech_recognition_postprocessing;

import com.google.common.collect.ImmutableMap;

import java.util.Arrays;
import java.util.Map;

public class GermanNumbers {
    private static Map<String, Integer> basicNumberTokens =
            ImmutableMap.<String, Integer>builder()
                    .put("null", 0)
                    .put("eins", 1)
                    .put("ein", 1)
                    .put("zwei", 2)
                    .put("drei", 3)
                    .put("vier", 4)
                    .put("fünf", 5)
                    .put("sechs", 6)
                    .put("sieben", 7)
                    .put("acht", 8)
                    .put("neun", 9)
                    .put("zehn", 10)
                    .put("elf", 11)
                    .put("zwölf", 12)
                    .put("sechzehn", 16)
                    .put("siebzehn", 17)
                    .put("zwanzig", 20)
                    .put("dreißig", 30)
                    .put("sechzig", 60)
                    .put("siebzig", 70)
                    .put("hundert", 100).build();

    private static Map<String, Integer> nonNumberKeywords =
            ImmutableMap.<String, Integer>builder()
                    .put("nicht", 0)
                    .put("wenig", 20)
                    .put("gering", 20)
                    .put("mittel", 50)
                    .put("stark", 80)
                    .put("extrem", 100).build();

    /**
     * Tries to parse a number between 0 and 100 written in German.
     * Returns NaN if no number could be recognized.
     *
     * @param numberString
     * @return an integer between 0 and 100, or NaN.
     */
    public static double parseGermanInteger(String numberString) {
        if (basicNumberTokens.containsKey(numberString)) {
            return basicNumberTokens.get(numberString);
        }

        if (nonNumberKeywords.containsKey(numberString)) {
            return nonNumberKeywords.get(numberString);
        }

        if (numberString.contains("und")) {
            String[] substrings = numberString.split("und");
            return Arrays.stream(substrings).mapToDouble(GermanNumbers::parseGermanInteger).sum();
        }

        if (numberString.endsWith("zehn")) {
            return 10 + parseGermanInteger(numberString.substring(0, numberString.length() - 4));
        }

        if (numberString.endsWith("zig")) {
            return 10 * parseGermanInteger(numberString.substring(0, numberString.length() - 3));
        }

        // couldn't parse it
        return Double.NaN;
    }
}
