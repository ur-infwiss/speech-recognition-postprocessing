package de.ur.iw.optapeb.speech_recognition_postprocessing;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import rx.subjects.PublishSubject;
import ws.wamp.jawampa.WampClient;
import ws.wamp.jawampa.WampClientBuilder;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;

import java.io.Closeable;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class WAMPConnection implements Closeable {
    private final static Logger LOG = LogManager.getLogger(WAMPConnection.class);

    private static final String REALM = "vtplus.vr";

    private static final Map<String, Event> selfReportTypeToWAMPEvent = ImmutableMap.of(
            "angst", Event.UPDATE_GRAPH_SELFREPORT_FEAR,
            "aufmerksamkeit", Event.UPDATE_GRAPH_SELFREPORT_ATTENTION,
            "sicherheitsverhalten", Event.UPDATE_GRAPH_SELFREPORT_SECURE);

    static {
        Preconditions.checkState(selfReportTypeToWAMPEvent.keySet().equals(Main.specificSelfReports));
    }

    private final WampClient client;

    public Function<Void, String> onRequestStatus = ((none) -> "ok");

    public Function<String, Void> onLastSelfReport = ((String selfReportType) -> null);

    public Function<String, Void> onNextSelfReport = ((String selfReportType) -> null);

    // the first value published via this subject causes all subscriptions to unsubscribe which in turn causes wamp
    // to unregister all events and procedures
    private final PublishSubject<Void> terminateAllSubscriptionsSubject = PublishSubject.create();

    public WAMPConnection(String wampRouterHostname, int wampRouterPort) throws Exception {

        // Create a builder and configure the client
        WampClientBuilder builder = new WampClientBuilder();
        NettyWampClientConnectorProvider connectorProvider = new NettyWampClientConnectorProvider();
        builder.withConnectorProvider(connectorProvider)
                .withUri("ws://" + wampRouterHostname + ":" + wampRouterPort + "/ws")
                .withRealm(REALM)
                .withInfiniteReconnects()
                .withReconnectInterval(2, TimeUnit.SECONDS);
        // Create a client through the builder. This will not immediately start
        // a connection attempt
        client = builder.build();
        // exceptions that will be thrown in case of invalid configuration

        client.statusChanged().subscribe((WampClient.State newState) -> {
            LOG.info("new state: {}", newState.getClass().getSimpleName());
            if (newState instanceof WampClient.ConnectedState) {
                // Client got connected to the remote router
                // and the session was established

                client.registerProcedure(Procedure.SPEECHRECOGNITION_REQUESTSTATUS.toString()).subscribe(
                        request -> {
                            LOG.info("Speech recognition status was queried.");
                            String result = onRequestStatus.apply(null);
                            request.reply(result);
                        },
                        throwable -> {
                            String errorMessage = "An error occurred during the registration of the " +
                                    "SPEECHRECOGNITION_REQUESTSTATUS procedure. This may be caused by two instances of this " +
                                    "program running simultaneously.";
                            LOG.error(errorMessage, throwable);
                        });
                client.registerProcedure(Procedure.LAST_SELFREPORT.toString()).subscribe(
                        request -> {
                            LOG.info("Type of last self-report received.");
                            String firstParameter = request.arguments().get(0).textValue();
                            Void result = onLastSelfReport.apply(firstParameter);
                            request.reply(result);
                        },
                        throwable -> {
                            String errorMessage = "An error occurred during the registration of the " +
                                    "LAST_SELFREPORT procedure. This may be caused by two instances of this " +
                                    "program running simultaneously.";
                            LOG.error(errorMessage, throwable);
                        });
                client.registerProcedure(Procedure.NEXT_SELFREPORT.toString()).subscribe(
                        request -> {
                            LOG.info("Type of next self-report received.");
                            String firstParameter = request.arguments().get(0).textValue();
                            Void result = onNextSelfReport.apply(firstParameter);
                            request.reply(result);
                        },
                        throwable -> {
                            String errorMessage = "An error occurred during the registration of the " +
                                    "NEXT_SELFREPORT procedure. This may be caused by two instances of this " +
                                    "program running simultaneously.";
                            LOG.error(errorMessage, throwable);
                        });

            } else if (newState instanceof WampClient.DisconnectedState) {
                // Client got disconnected from the remote router
                // or the last possible connect attempt failed
            } else if (newState instanceof WampClient.ConnectingState) {
                // Client starts connecting to the remote router
            }
        });

        LOG.info("Connecting to {}...", client.routerUri());
        client.open();
    }

    @Override
    public void close() {
        // Wait synchronously for the client to close
        client.close().toBlocking().last();
    }

    private void callProcedure(String procedure, Object... args) {
        LOG.info("Calling RPC " + procedure + " with args " + Arrays.toString(args));
        client.call(procedure, args).subscribe(
                result -> {
                    LOG.info("Called RPC " + procedure + " with args " + Arrays.toString(args));
                },
                throwable -> {
                    if (throwable.getMessage().contains("no_such_procedure")) {
                        LOG.error("Error during RPC " + procedure + ": " + throwable.getMessage());
                    } else {
                        LOG.error("Error during RPC " + procedure, throwable);
                    }
                });
    }

    private void publishEvent(String topic, Object... args) {
        LOG.info("Publishing Event " + topic + " with args " + Arrays.toString(args));
        client.publish(topic, args).subscribe(
                result -> {
                    LOG.info("Published Event " + topic + " with args " + Arrays.toString(args));
                },
                throwable -> {
                    LOG.error("Error during Event " + topic, throwable);
                });
    }

    public void callDetectedSpeechNumber(double number) {
        callProcedure(Procedure.EC_DETECTEDSPEECHNUMBER.toString(), number);
        callProcedure(Procedure.EG_DETECTEDSPEECHNUMBER.toString(), number);
    }

    public void appendToSelfReportGraph(String selfReportType, double number) {
        var event = selfReportTypeToWAMPEvent.get(selfReportType);
        publishEvent(event.toString(), number);
    }

    public void callDetectedYes() {
        callProcedure(Procedure.EC_DETECTEDSPEECHYES.toString());
        callProcedure(Procedure.EG_DETECTEDSPEECHYES.toString());
    }

    public void callDetectedNo() {
        callProcedure(Procedure.EC_DETECTEDSPEECHNO.toString());
        callProcedure(Procedure.EG_DETECTEDSPEECHNO.toString());
    }

    private enum Procedure {
        SPEECHRECOGNITION_REQUESTSTATUS("de.speechrecognition.requestStatus"),
        LAST_SELFREPORT("de.ur.selbstbericht"),
        NEXT_SELFREPORT("de.ur.nextSelfreport"),
        EG_DETECTEDSPEECHNUMBER("de.ur.eg.detectedSpeechNumber"),
        EC_DETECTEDSPEECHNUMBER("eu.vtplus.ec.detectedSpeechNumber"),
        EG_DETECTEDSPEECHYES("de.ur.eg.detectedSpeechYes"),
        EC_DETECTEDSPEECHYES("eu.vtplus.ec.detectedSpeechYes"),
        EG_DETECTEDSPEECHNO("de.ur.eg.detectedSpeechNo"),
        EC_DETECTEDSPEECHNO("eu.vtplus.ec.detectedSpeechNo");

        private final String procedureString;

        Procedure(String procedureString) {
            this.procedureString = procedureString;
        }

        @Override
        public String toString() {
            return procedureString;
        }
    }
    private enum Event {
        UPDATE_GRAPH_SELFREPORT_FEAR("de.eg.sb.fearGraph"),
        UPDATE_GRAPH_SELFREPORT_ATTENTION("de.eg.sb.attentionGraph"),
        UPDATE_GRAPH_SELFREPORT_SECURE("de.eg.sb.secureGraph");

        private final String eventString;

        Event(String eventString) {
            this.eventString = eventString;
        }

        @Override
        public String toString() {
            return eventString;
        }
    }
}
