package de.ur.iw.optapeb.speech_recognition_postprocessing;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;
import sccn.LSL;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main implements CommandLine.IVersionProvider {
    private final static Logger LOG = LogManager.getLogger(Main.class);

    @Override
    public String[] getVersion() {
        String v = getClass().getPackage().getImplementationVersion();
        if (v == null) {
            return new String[]{"DEV"};
        } else {
            return new String[]{v};
        }
    }

    private static final CommandLineParameters parameters = new CommandLineParameters();

    private static LSL.StreamOutlet lslOutletSpokenNumbers;
    private static LSL.StreamOutlet lslOutletTurns;
    private static final Map<String, LSL.StreamOutlet> lslOutletsSelfReports = new HashMap<>();

    private static WAMPConnection wampConnection;

    public static final Set<String> specificSelfReports = ImmutableSet.of(
            "angst", "aufmerksamkeit", "sicherheitsverhalten");

    /**
     * Contains the lest successfully parsed number.
     */
    private static double lastNumber = Double.NaN;
    /**
     * For each type of self-report, this contains the last labelled value.
     * This data is used when the user says something like "gleich geblieben" to
     * retrieve the user's last value and use that again.
     */
    private static final Map<String, Double> lastSelfReports = new HashMap<>(specificSelfReports.size());

    /**
     * Contains either <code>null</code> or one of the keys of
     * specificSelfReports. Used as a key for <code>lastSelfReports</code>.
     */
    private static String nextSelfReportType = null;

    private static final Set<String> noSpeechKeywords =
            ImmutableSet.of("nspc");

    /**
     * @param turn
     * @param keywords
     * @return <code>true</code> if <code>turn</code> contains any one of the Strings in <code>keywords</code>, otherwise <code>false</code>>.
     */
    private static boolean turnContainsAnyKeyword(String turn, Set<String> keywords) {
        for (String keyword : keywords) {
            if (turn.contains(keyword)) {
                return true;
            }
        }
        return false;
    }

    private static void handleRecognizedNumber(double number) {
        LOG.info("Recognized number: " + (int) number);
        lastNumber = number;
        LOG.info("Pushing " + number + " to LSL stream: " + lslOutletSpokenNumbers.info().name() + " (" + lslOutletSpokenNumbers.info().type() + ")");
        lslOutletSpokenNumbers.push_sample(new double[]{lastNumber});
        // no longer used:
        //wampConnection.callDetectedSpeechNumber(number);
    }

    private static Void handleLastSelfReportType(String reportedType) {
        for (String selfReportType : specificSelfReports) {
            if (reportedType.toLowerCase().contains(selfReportType)) {
                lastSelfReports.put(selfReportType, lastNumber);
                lslOutletsSelfReports.get(selfReportType).push_sample(new double[]{lastNumber});
                wampConnection.appendToSelfReportGraph(selfReportType, lastNumber);
            }
        }
        nextSelfReportType = null;
        return null;
    }

    private static Void handleNextSelfReportType(String reportedType) {
        for (String selfReportType : specificSelfReports) {
            if (reportedType.toLowerCase().contains(selfReportType)) {
                nextSelfReportType = selfReportType;
            }
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        // force LSL's native library to load at startup -> fail fast
        LSL.local_clock();

        // parse command line; maybe quit here
        CommandLine commandLine = new CommandLine(parameters);
        try {
            commandLine.parseArgs(args);
        } catch (CommandLine.PicocliException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println();
            commandLine.usage(System.out);
            return;
        }
        if (commandLine.isUsageHelpRequested()) {
            commandLine.usage(System.out);
            return;
        } else if (commandLine.isVersionHelpRequested()) {
            commandLine.printVersionHelp(System.out);
            return;
        }

        // LSL out
        openLSLOutlets();

        // LSL in: find and open Kaldi/Zamia LSL stream with recognized turns
        var turnsReader = new SpeechTurnsReader(parameters.lslPredicateTurns);
        var readerThread = new Thread(turnsReader);
        readerThread.start();
        var turnsQueue = turnsReader.getDataQueue();

        // WAMP
        wampConnection = new WAMPConnection(
                parameters.wampRouterHostname, parameters.wampRouterPort);
        wampConnection.onLastSelfReport = Main::handleLastSelfReportType;
        wampConnection.onNextSelfReport = Main::handleNextSelfReportType;

        // main loop: try to parse numbers or find keywords
        do {
            String turn = turnsQueue.take(); // blocking read
            turn = turn.trim();

            List<String> words = Lists.asList(turn, turn.split("\\s++"));
            var maybeNumber = words.stream()
                    .mapToDouble(GermanNumbers::parseGermanInteger)
                    .filter(n -> !Double.isNaN(n))
                    .findFirst();
            if (maybeNumber.isPresent()) { // it is a number
                handleRecognizedNumber(maybeNumber.getAsDouble());

            } else if (turnContainsAnyKeyword(turn, noSpeechKeywords)) {
                // "no speech" - ignore
                continue;

            } else if (turnContainsAnyKeyword(turn, GermanKeywords.noChange)) {
                LOG.info("Detected 'same as last time'");
                if (nextSelfReportType != null && lastSelfReports.containsKey(nextSelfReportType)) {
                    double lastReportedNumber = lastSelfReports.get(nextSelfReportType);
                    LOG.info("Last self-report " + nextSelfReportType + " was " + lastReportedNumber + " - using that again.");
                    handleRecognizedNumber(lastReportedNumber);
                }

            } else if (turnContainsAnyKeyword(turn, GermanKeywords.yes)) {
                LOG.info("Detected YES");
                lslOutletTurns.push_sample(new String[]{"YES"});
                wampConnection.callDetectedYes();

            } else if (turnContainsAnyKeyword(turn, GermanKeywords.no)) {
                LOG.info("Detected NO");
                lslOutletTurns.push_sample(new String[]{"NO"});
                wampConnection.callDetectedNo();
            }

        } while (true);
    }

    private static void openLSLOutlets() throws IOException {
        lslOutletSpokenNumbers = new LSL.StreamOutlet(new LSL.StreamInfo(
                "NumberParser",
                "SpokenNumbers",
                1,
                LSL.IRREGULAR_RATE,
                LSL.ChannelFormat.double64,
                "uid30458"));

        lslOutletTurns = new LSL.StreamOutlet(new LSL.StreamInfo(
                "Turns CalibrationSzenario",
                "turns",
                1,
                LSL.IRREGULAR_RATE,
                LSL.ChannelFormat.string,
                "uid092384875"));

        // open LSL streams for all known types of self-report
        for (String selfReport : specificSelfReports) {
            lslOutletsSelfReports.put(
                    selfReport,
                    new LSL.StreamOutlet(new LSL.StreamInfo(
                            "SELBSTBERICHT_" + selfReport.toUpperCase(),
                            "SpokenNumbers",
                            1,
                            LSL.IRREGULAR_RATE,
                            LSL.ChannelFormat.double64,
                            "uid" + selfReport.hashCode())));
        }
    }
}
