
# Post-Processing für die Spracherkennung

## Verwendungszweck

Dieses Programm bietet mehrere kleine Dienste rund um den Ausgabe-Datenstrom des Spracherkenners. V.a. parst es deutsche Zahlenworte und bestimmte Schlüsselworte und teilt das der Außenwelt per LSL und WAMP mit.
Zur Ausführung wird mindestens **Java 13** benötigt. (ein Build für niedrigere Versionen wäre momentan (Stand August 2020) auf Anfrage auch denkbar)

## Einstieg

Programm starten: Im Unterordner \build\launch4j die Konsolenanwendung ``speech-recognition-postprocessing.exe`` starten.

Es sind keine Parameter für den Start erforderlich. Für eine Erklärung aller unterstützten Kommandozeilenparameter kann das Programm mit dem Parameter ``--help`` gestartet werden. Hauptsächlich kann die Verbindung zum WAMP-Router und der zu lesende LSL-Stream beeinflusst werden.

## Ausgaben

 * Sobald "... new state: ConnectedState" angezeigt wird, ist die Verbindung zum WAMP Router aufgebaut.
 * Vom Spracherkenner empfangene Turns - auch solche, in denen nichts erkannt wird - tauchen im Konsolen-Log auf.
 * Ausgegebene Fehler führen (im Allgemeinen) nicht zum Abbruch des Programms, insbesondere nicht eventuelle Meldungen bzgl. "wamp.error.no_such_procedure" bei fehlenden WAMP-RPCs.

## Arbeitsweise

Die erkannten Turns vom Spracherkenner werden in einer Endlosschleife evaluiert. Das Programm muss extern beendet werden.
